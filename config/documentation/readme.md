> "Documentation is like sex:  
> when it is good, it is very, very good;  
> and when it is bad, it is better than nothing."  
> -- Dick H. Brandon

## Documentation Generators

## C
- [Doxygen](http://www.stack.nl/~dimitri/doxygen/manual/starting.html#extract_all)
- [doxygen/doxygen](https://github.com/doxygen/doxygen)
- Doxygen is the de facto standard tool for generating documentation from annotated C++ sources, but it also supports other popular programming languages such as C, Objective-C, C#, PHP, Java, Python, IDL (Corba, Microsoft, and UNO/OpenOffice flavors), Fortran, VHDL, Tcl, and to some extent D.

## Elixir
- [elixir-lang/ex_doc](https://github.com/elixir-lang/ex_doc) - ExDoc produces HTML and EPUB documentation for Elixir projects.

### Haxe
- [HaxeFoundation/dox](https://github.com/HaxeFoundation/dox) - Haxe documentation generator.

## Other Resources
- [unicodeveloper/awesome-documentation-tools](https://github.com/unicodeveloper/awesome-documentation-tools)
