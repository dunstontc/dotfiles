# compdef tag
# tag - A tool for manipulating and querying file tags.
# usage:
#   tag -a | --add <tags> <path>...     Add tags to file
#   tag -r | --remove <tags> <path>...  Remove tags from file
#   tag -s | --set <tags> <path>...     Set tags on file
#   tag -m | --match <tags> <path>...   Display files with matching tags
#   tag -l | --list <path>...           List the tags on file
#   tag -f | --find <tags> <path>...    Find all files with tags (-A, -e, -R ignored)
# <tags> is a comma-separated list of tag names; use * to match/find any tag.

_tag() {

	_arguments \
		"-b[Display network rate in Byte per second]" \
		"-B[Bind server to the given IP or host NAME]:host:_hosts" \
		"-c[Connect to a Glances server]:host:_hosts" \
		"-C[Path to the configuration file]:configuration path:_files -/" \
		"-d[Disable disk I/O module]" \
		"-e[Enable the sensors module (Linux-only)]" \
		"-f[Set the output folder (HTML) or file (CSV)]:output path:_files -/" \
		"-h[Display the syntax and exit]" \
		"-m[Disable mount module]" \
		"-n[Disable network module]" \
		"-o[Define additional output (available: HTML or CSV)]:output type:(HTML CSV)" \
		"-p[Define the client or server TCP port (default: 61209)]:port:_ports" \
		"-P[Client/server password]:password:" \
		"-s[Run Glances in server mode]" \
		"-t[Set the refresh time in seconds (default: 3)]:seconds:" \
		"-v[Display the version and exit]" \
		"-z[Do not use the bold color attribute]" \
}

_tag "$@"



        -v | --version      Display version
        -h | --help         Display this help
        -A | --all          Display invisible files while enumerating
        -e | --enter        Enter and enumerate directories provided
        -R | --recursive    Recursively process directories
        -n | --name         Turn on filename display in output (default)
        -N | --no-name      Turn off filename display in output (list, find, match)
        -t | --tags         Turn on tags display in output (find, match)
        -T | --no-tags      Turn off tags display in output (list)
        -g | --garrulous    Display tags each on own line (list, find, match)
        -G | --no-garrulous Display tags comma-separated after filename (default)
        -H | --home         Find tagged files in user home directory
        -L | --local        Find tagged files in home + local filesystems
        -R | --network      Find tagged files in home + local + network filesystems
        -0 | --nul          Terminate lines with NUL (\0) for use with xargs -0


# vim: ft=zsh sw=2 ts=2 et
