module.exports = {
  'brew': [
    'bash',
    'neovim',
    'python',
    'reattach-to-user-namespace',
    'ruby',
    'sqlite3',
    'tmux',
    'vim',
  ],
  'gem': [
    'json',
    'json_pure',
    'lolcat',
    'neovim',
  ],
  'npm': [
    'cson',
    'eslint',
    'eslint-plugin-vue',
    'gulp-cli',
    'jq.node',
    'npm',
    'tern',
    'yo',
  ],
  'pip3': [
    'glances',
    'jrnl',
    'neovim',
    'proselint',
    'Pygments',
    'vim-vint',
    'yamllint',
  ],
  'Apps': [
    'Adobe Illustrator CC',
    'Adobe Photoshop CC',
    'Alfred 3',
    'Atom',
    'BetterTouchTool',
    'DB Browser for SQLite',
    'IconJar',
    'iTerm2',
    'KeyCastr',
    'MongoDB Compass',
    'MySQL Workbench',
    'VLC',
    'VSCode',
    'Yoink',
  ],
  'Atom': [
    'b3by/atom-clock',
    'Osmose/advanced-open-file',
    'dinhani/atom-section-divider',
    'heartbeat-med/command-history',
    'aki77/atom-disable-keybindings',
    'mkxml/atom-filesize',
    'sindresorhus/atom-editorconfig',
    'Yatoom/emmet-atom-simplified',
    'file-icons/atom',
    'abrookins/fuzzy-theme-switcher',
    'facebook-atom/hyperclick',
    'UziTech/hyperlink-hyperclick',
    'tgandrews/atom-easy-jsdoc',
    'burodepeper/language-markdown',
    'Alhadis/language-viml',
    'tmux-plugins/language-tmux',
    'rapgenic/menu-editor',
    'hedefalk/atom-vue',
    'abe33/atom-pigments',
    'danielbayley/atom-modular-keymaps',
    'izuzak/atom-pdf-view',
    'platformio/platformio-atom-ide-terminal',
    'danielbrodin/atom-project-manager',
    'mrodalgaard/atom-todo-show',
  ],
  'VS Code': [
    'formulahendry/vscode-auto-close-tag',
    // 'formulahendry/vscode-auto-rename-tag', // https://github.com/formulahendry/vscode-auto-rename-tag/issues,
    'HookyQR/VSCodeBeautify',
    'editorconfig/editorconfig-vscode',
    'Microsoft/vscode-npm-scripts',
    'ChristianKohler/PathIntellisense',
    'alefragnani/vscode-project-manager',
    'dinhani/vscode-section-divider',
    'cssho/vscode-svgviewer',
    'wayou/vscode-todo-highlight',
    'wholroyd/vscode-jinja',
    'neilsustc/vscode-markdown',
    'DonJayamanne/pythonVSCode',
    'rubyide/vscode-ruby',
    'd4rkr00t/language-stylus',
    'fallenwood/vscode-viml',
    'vuejs/vetur',
    'wesbos/cobalt2-vscode',
    'liamsheppard/voodoo-theme',
    'Tyriar/vscode-theme-sapphire',
    'file-icons/vscode',
    'vscode-icons/vscode-icons',
  ]
};
